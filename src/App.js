import React, { Component } from 'react';
import Wax from 'wax/packages/wax-react'

class App extends Component {
  render() {
    return (
      <div className="App">
      <Wax content='test'/>
      </div>
    );
  }
}

export default App;
